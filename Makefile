NAME=blockmon
BIN=$(NAME).exe
#PAWN_OBJ=third_party/pawn/amx/amx.obj third_party/pawn/amx/amxcore.obj third_party/pawn/amx/amxcons.obj
OBJ=src/main.obj src/image.obj src/file_mzm.obj src/file_pal.obj #$(PAWN_OBJ)
DISTDIR=dist

CC=i586-pc-msdosdjgpp-gcc
CFLAGS=
LDFLAGS=
PWD=$(CURDIR)

$(BIN): $(OBJ)
	$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS)

%.obj: %.c
	$(CC) $(CFLAGS) -c $< -o $@

pkg: $(BIN) $(OBJ)
	mkdir -p $(DISTDIR)/progs/$(NAME)/
	mkdir -p $(DISTDIR)/source/$(NAME)/src/
	mkdir -p $(DISTDIR)/appinfo/
	cp $(BIN) $(DISTDIR)/progs/$(NAME)/
	cp $(NAME).lsm $(DISTDIR)/appinfo/
	cp Makefile $(NAME).lsm $(DISTDIR)/source/$(NAME)/
	cp src/*.c $(DISTDIR)/source/$(NAME)/src/
	cp src/*.h $(DISTDIR)/source/$(NAME)/src/
	cd $(DISTDIR) && 7za a -mm=deflate -mx=9 -tzip $(PWD)/$(NAME).zip *

clean:
	rm -f $(BIN) $(OBJ)
	rm -r $(NAME).zip $(DISTDIR)

cwsdpmi.exe:
	wget http://www.delorie.com/pub/djgpp/current/v2misc/csdpmi7b.zip
	unzip -L -j csdpmi7b.zip bin/cwsdpmi.exe
	rm csdpmi7b.zip

emu: $(BIN) cwsdpmi.exe
	dosbox $^
