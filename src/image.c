#include <stdio.h>
#include <malloc.h>
#include "util.h"
#include "image.h"

struct image * image_create(size_t width, size_t height)
{
    struct image * img;
    size_t buf_size;

    img = (struct image *) malloc(sizeof(*img));
    if (!img) {
        ERROR("Cannot allocate memory for image handle");
        return NULL;
    }

    buf_size = width * height * sizeof(*(img->buf));
    img->width = width;
    img->height = height;
    img->buf = malloc(buf_size);
    if (!(img->buf)) {
        ERROR("Cannot allocate memory for image buffer");
        free(img);
        img = NULL;
    }

    return img;
}

void image_free(struct image * img)
{
    if (!img) return;

    if (img->buf) free(img->buf);
    free(img);

    return;
}

