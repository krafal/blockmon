#include <stdio.h>
#include <conio.h>
#include <pc.h>
#include "util.h"
#include "image.h"
#include "file_mzm.h"
#include "file_pal.h"

int main(int argc, char *argv[])
{
    int mode, x, y;
    int can_mod_pal = 0;
    int can_mod_char = 0;
    //struct videoconfig vc;
    struct image *image;
    struct symbol b;

    if (argc != 3) {
        printf("usage: %s filename.mzm filename.pal\n", argv[0]);
        return 1;
    }

    printf("blockmon %s %s\n", __DATE__, __TIME__);
    printf("Copyright (C) 2022, Rafal Kolucki\n");

    if (argc == 3) {
        /* load image */
        image = mzm_load(argv[1]);
        if (image == NULL) {
            ERROR("Couldn't load file");
            return 1;
        }

        getch();

        textmode(C80);
        _set_screen_lines(28);
        _setcursortype(_NOCURSOR);
        intensevideo();

        /* load palette */
        pal_load(argv[2]);

        for (y=0; y<(image->height); y++) {
            for (x=0; x<(image->width); x++) {
                b = image->buf[y * (image->width) + x];
                ScreenPutChar(b.character, b.attributes, x, y);
            }
        }

        getch();

        textmode(LASTMODE);

        image_free(image);
    }

    return 0;
}
