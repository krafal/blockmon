#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include "util.h"
#include "file_mzm.h"
#include "image.h"


struct image * mzm_load_layer(struct image *image, const mzm_block_layer_t *layer)
{
    size_t pos;
    int x, y;

    if ((image == NULL) || (layer == NULL)) {
        return NULL;
    }

    for (y = 0; y < image->height; y++) {
        for (x = 0; x < image->width; x++) {
            pos = (y * image->width) + x;
            (image->buf)[pos].character = layer[pos].character;
            (image->buf)[pos].attributes = layer[pos].color;
        }
    }

    return image;
}

struct image * mzm_load(const char filename[])
{
    mzm_type_t type = MZM_UNKNOWN;
    mzm_storage_mode_t stor_mode;
    mzm_header_t header;
    size_t width, height;
    size_t tmp_size = 0;
    size_t data_len = 0;
    void * data_buf = NULL;
    FILE * mzm_file = NULL;
    struct image * new_image = NULL;

    /* Open MZM file */
    mzm_file = fopen(filename, "rb");
    if (!mzm_file) {
        ERROR("Cannot open file");
        goto end;
    }

    /* Try reading magic number */
    tmp_size = fread(&header, 1, sizeof(header.magic), mzm_file);
    if (tmp_size != sizeof(header.magic)) {
        ERROR("Cannot read magic from file");
        goto free_file;
    }
    rewind(mzm_file);

    /* Detect file format with magic number */
    if (strncmp(header.magic, "MZMX", sizeof(header.magic)) == 0) {
        type = MZM_MZMX;
        tmp_size = fread(&header, 1, sizeof(header.mzmx), mzm_file);
        width = header.mzmx.width;
        height = header.mzmx.height;
        stor_mode = MZM_STORAGE_BOARD;
        printf("MZMx: %dx%d\n",
            header.mzmx.width, header.mzmx.height);
    }
    else if (strncmp(header.magic, "MZM2", sizeof(header.magic)) == 0) {
        type = MZM_MZM2;
        tmp_size = fread(&header, 1, sizeof(header.mzm2), mzm_file);
        width = header.mzm2.width;
        height = header.mzm2.height;
        switch (header.mzm2.storage_mode) {
        case 0:
            stor_mode = MZM_STORAGE_BOARD;
            break;
        case 1:
            stor_mode = MZM_STORAGE_LAYER;
            break;
        default:
            break;
        }
        printf("MZM2: %dx%d, robot %d@0x%x, stor %d, is a savegame? %d\n",
            header.mzm2.width, header.mzm2.height,
            header.mzm2.robot_count, header.mzm2.robot_offset,
            header.mzm2.storage_mode, header.mzm2.savegame);
    }
    else if (strncmp(header.magic, "MZM3", sizeof(header.magic)) == 0) {
        type = MZM_MZM3;
        tmp_size = fread(&header, 1, sizeof(header.mzm3), mzm_file);
        width = header.mzm3.width;
        height = header.mzm3.height;
        switch (header.mzm3.storage_mode) {
        case 0:
            stor_mode = MZM_STORAGE_BOARD;
            break;
        case 1:
            stor_mode = MZM_STORAGE_LAYER;
            break;
        default:
            break;
        }
        printf("MZM3: %dx%d, robot %d@0x%x, stor %d, is a savegame? %d, world ver %d\n",
            header.mzm3.width, header.mzm3.height,
            header.mzm3.robot_count, header.mzm3.robot_offset,
            header.mzm3.storage_mode, header.mzm3.savegame, header.mzm3.world_version);
    }
    else {
        ERROR("Invalid/unsupported MZM file");
        goto free_file;
    }

    /* Calculate board/layer data buffer size */
    data_len = width * height;
    switch (stor_mode) {
    case MZM_STORAGE_BOARD:
        ERROR("Unsupported storage mode, export from overlay!");
        goto free_file;
        break;
    case MZM_STORAGE_LAYER:
        data_len *= sizeof(mzm_block_layer_t);
        break;
    default:
        ERROR("Unknown storage mode");
        goto free_file;
        break;
    }

    /* Allocate board/layer data buffer */
    data_buf = malloc(data_len);
    if (!data_buf) goto free_file;

    tmp_size = fread(data_buf, 1, data_len, mzm_file);
    if (tmp_size != data_len) {
        ERROR("Not enough bytes copied to data buffer");
        goto free_data;
    }

    /* Allocate image */
    new_image = image_create(width, height);
    if (!new_image) goto free_data;

    /* Read from buffer to image */
    switch (stor_mode) {
    case MZM_STORAGE_LAYER:
        if (!mzm_load_layer(new_image, data_buf)) {
            goto free_image;
        }
        break;
    default:
        ERROR("Unsupported storage mode, export from overlay!");
        goto free_image;
        break;
    }

    goto free_data;

free_image:
    image_free(new_image);
    new_image = NULL;
free_data:
    free(data_buf);
    data_len = 0;
free_file:
    fclose(mzm_file);
end:
    return new_image;
}
