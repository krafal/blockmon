#ifndef __file_mzm_h
#define __file_mzm_h

#include <stdint.h>

typedef enum mzm_type_e { MZM_UNKNOWN, MZM_MZMX, MZM_MZM2, MZM_MZM3 } mzm_type_t;
typedef enum mzm_storage_mode_e { MZM_STORAGE_BOARD, MZM_STORAGE_LAYER } mzm_storage_mode_t;

typedef struct {
    char magic[4];
    uint8_t width;
    uint8_t height;
    uint8_t unused[10];
} __attribute__((packed)) mzmx_header_t;

typedef struct {
    char magic[4];
    uint16_t width;
    uint16_t height;
    uint32_t robot_offset;
    uint8_t robot_count;
    uint8_t storage_mode;
    uint8_t savegame;
    uint8_t unused;
} __attribute__((packed)) mzm2_header_t;

typedef struct {
    char magic[4];
    uint16_t width;
    uint16_t height;
    uint32_t robot_offset;
    uint8_t robot_count;
    uint8_t storage_mode;
    uint8_t savegame;
    uint16_t world_version;
    uint8_t reserved[3];
} __attribute__((packed)) mzm3_header_t;

typedef union {
    char magic[4];
    mzmx_header_t mzmx;
    mzm2_header_t mzm2;
    mzm3_header_t mzm3;
} mzm_header_t;

typedef struct {
    uint8_t id;
    uint8_t param;
    uint8_t color;
    uint8_t under_id;
    uint8_t under_param;
    uint8_t under_color;
} __attribute__((packed)) mzm_block_board_t;

typedef struct {
    uint8_t character;
    uint8_t color;
} __attribute__((packed)) mzm_block_layer_t;

struct image * mzm_load_board(struct image *image, const mzm_block_board_t *board);
struct image * mzm_load_layer(struct image *image, const mzm_block_layer_t *layer);
struct image * mzm_load(const char filename[]);

#endif
