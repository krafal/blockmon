#ifndef __image_h
#define __image_h

#include <stdint.h>

struct symbol {
    uint8_t character;
    uint8_t attributes;
};

struct image {
    size_t width;
    size_t height;
    struct symbol *buf;
};

#define IMAGE_BUF_SIZE(i) (i->width * i->height * sizeof(*(i->buf)))

struct sprite {
    struct image * image;
    size_t width;
    size_t height;
    size_t offset_x;
    size_t offset_y;
};

struct image * image_create(size_t width, size_t height);
void image_free(struct image * img);

#endif
