#include <stdio.h>
#include <stdint.h>
#include <dos.h>
#include <conio.h>

#include "util.h"
#include "file_pal.h"

int pal_load(const char filename[])
{
    FILE *pal_file = NULL;
    size_t pal_len, i;
    uint8_t pal_buf[3];
    uint8_t pal_vga_offset[] = {0x00, 0x01, 0x02, 0x03, 
                                0x04, 0x05, 0x14, 0x07, 
                                0x38, 0x39, 0x3a, 0x3b, 
                                0x3c, 0x3d, 0x3e, 0x3f};

    /* Open PAL file */
    pal_file = fopen(filename, "rb");
    if (!pal_file) {
        ERROR("Cannot open palette file");
        return 0;
    }

    fseek(pal_file, 0L, SEEK_END);
    pal_len = ftell(pal_file) / sizeof(pal_buf);
    rewind(pal_file);

    for (i = 0; i < pal_len; i++) {
        fread(pal_buf, 1, sizeof(pal_buf)/sizeof(pal_buf[0]), pal_file);
        outportb(0x3c8, pal_vga_offset[i]);
        outportb(0x3c9, pal_buf[0]);
        outportb(0x3c9, pal_buf[1]);
        outportb(0x3c9, pal_buf[2]);
    }

    fclose(pal_file);

    return 1;
}
